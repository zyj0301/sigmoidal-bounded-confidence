.PHONY: cliques
.PHONY: figs

cliques: fig/paired-cliques-aligned.png fig/paired-cliques-misaligned.png

figs:  fig/diagram-1.png fig/diagram-0.png fig/karate.png  fig/path-graph.png cliques

fig/diagram-0.png: scripts/diagram-0.R
	Rscript scripts/diagram-0.R

fig/diagram-1.png: scripts/diagram-1.R
	Rscript scripts/diagram-1.R

fig/karate.png: scripts/perturbations.jl
	julia --project=. scripts/perturbations.jl
	
# NOTE: recipe below assumes access to 20 threads, need to dial this way down if running on a laptop
throughput/paired-cliques: scripts/paired-clique-phase-diagram.jl scripts/paired-clique-phase-plane.jl
	julia --project=. --threads 20 scripts/paired-clique-phase-diagram.jl 
	julia --project=. --threads 20 scripts/paired-clique-phase-plane.jl

throughput/path-graph: scripts/path-graph-phase-diagram.jl 
	julia --project=. --threads 20 scripts/path-graph-phase-diagram.jl

fig/path-graph.png: throughput/path-graph scripts/path-graph-phase-diagram.R
	Rscript scripts/path-graph-phase-diagram.R

fig/paired-cliques-aligned.png: throughput/paired-cliques scripts/paired-clique-viz.R
	Rscript scripts/paired-clique-viz.R


