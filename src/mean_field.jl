


@with_kw mutable struct meanFieldState
    """
    A state of a mean field approximation for our dynamics. Can be constructed from full opinion_states. Has its own evolve! method for dynamics.
    G::Graph, the full graph substrate
    Z::Vector{Int64}, a grouping of nodes. Entry Z[i] gives the label of node i. Probably fine to do this in just two groups for now.
    x::The reduced state vector. Same length as the number of distinct values of Z
    """
    x::Vector{Float64}
    G::Graph
    Z::Vector{Int64}
    k::Vector{Int64} = Vector{Int64}()    # vector of discordant degrees
    d::Vector{Int64} = Vector{Int64}()    # vector of concordant degrees
    λ::Float64 = 0.5
end

function compute_degrees!(MF::meanFieldState)
    """
    Compute the concordant and discordant degrees of MF::meanFieldState, storying them in MF.
    Performs a simple loop over each node in G and its neighborhood.
    """
    n = nv(MF.G)
    k = zero(1:n)
    d = zero(1:n)
    for i = 1:n
        cord = (MF.Z[i] .== MF.Z[neighbors(MF.G, i)])
        k[i] = sum(1 .- cord)
        d[i] = sum(cord)
    end
    MF.k = k
    MF.d = d
end

function evolve!(MF::meanFieldState, f, g; timesteps = 1)
    """
    Dynamical operator for mean field approximations.
    MF::meanFieldState, the current state of the mean field system
    f::function, as in evolve!(::opinion_state)
    g::function, as in evolve!(::opinion_state)
    """

    λ = MF.λ

    x₀ = MF.x[1]
    x₁ = MF.x[2]

    k  = MF.k
    d  = MF.d

    Z = MF.Z

    S₀ = (Z .== 0)
    S₁ = (Z .== 1)

    m₀ = sum(S₀)
    m₁ = sum(S₁)

    # run dynamics
    for i = 1:timesteps

        # assumed symmetric and that f(x₀, x₀) = f(x₁, x₁)
        u  = f(x₀, x₀)
        v  = f(x₁, x₀)

        x̄₀ = λ*g(x₀)+(1.0-λ)/m₀*sum(S₀.*((x₀.*u.*d .+ x₁.*v.*k)./(u.*d + v.*k)))
        x̄₁ = λ*g(x₁)+(1.0-λ)/m₁*sum(S₁.*((x₁.*u.*d .+ x₀.*v.*k)./(u.*d + v.*k)))

        x₀, x₁  = x̄₀, x̄₁
    end

    # save state
    MF.x[1] = x₀
    MF.x[2] = x₁
end

function evolve_to_convergence!(MF::meanFieldState, f, g; tol = 10^(-8))
    """
    Iterate evolve!(::meanFieldState, f, g) until MF.x changes by no more than tol.
    """
    print("not implemented")
end

function MF_approx(S::opinion_state; cutoff::Float64 = 0.0, λ = 0.0)
    """
    S::opinion_state, a given opinion state used to initialize the MF approximation.
    cutoff::Float64, the cutoff used in the opinions x used to distinguish them into groups. Nodes i with x[i] ≦  cutoff will be grouped into group 0, while nodes i with x[i] > cutoff will be grouped into group 1.
    """

    X = stateVector(S)
    n = length(X)
    Z = repeat([0], n)

    Z[X .<= cutoff] .= 0
    Z[X .>  cutoff] .= 1

    x = [0.0, 0.0]
    x[1] = mean(X[Z.==0])
    x[2] = mean(X[Z.==1])

    MF = meanFieldState(x=x, G=S.G, Z=Z)

    # if λ is specified, set the MF value
    if λ != 0
        MF.λ = λ
    end

    compute_degrees!(MF)

    return MF
end

function stateVector(MF::meanFieldState)
    return MF.x
end
