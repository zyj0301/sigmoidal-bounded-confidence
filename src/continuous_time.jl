function weightMatrix(x, A; β = 1.0, γ = 0.0, δ = 0.5)
    """
    compute the weight matrix W given a state x, graph A, and parameters β, γ, and δ of the logistic sigmoid. 
    """
    # compute the sigmoid values between all pairs of nodes
    # possibly very wasteful
    dists = (x .- x') .^ 2
    n = length(x)
    W = logistic_sigmoid.(dists; β = β, γ = γ, δ = δ)
    W = W .* A
    return W
end

function weightMatrixWithDeriv(x, A; β = 1.0, γ = 0.0, δ = 0.5)
    """
    compute the weight matrix W given a state x, graph A, and parameters β, γ, and δ of the logistic sigmoid. 
    """
    # compute the sigmoid values between all pairs of nodes
    # possibly very wasteful
    dists = (x .- x') .^ 2
    n = length(x)
    W = logistic_sigmoid.(dists; β = β, γ = γ, δ = δ)
    W = W .* A
    
    ∂W = 2γ*W.*(1 .- W).*(x .- x')
    return W, ∂W
    
end

function dynamics!(dx, x, p, t)
    """
    should find a way to preallocate and update W
    """
    
    p[6][:] = weightMatrix(x, p[1]; β = p[3], γ = p[4], δ = p[5])
    p[6][:] = p[6] ./ sum(p[6], dims = 2)
    dx[:] = (1.0 .- p[2]) .* (-x + p[6]*x) 
end

function dynamics(x, A, z; β, γ, δ)
    W = weightMatrix(x, A; β = β, γ = γ, δ = δ)
    W = W ./ sum(W, dims = 2)
    return (1.0 .- z) .* (-x + W*x) 
end


function autoJacobian(x0, p)
    function dx(x, p)
        A, z, β, γ, δ = p
        W = weightMatrix(x, A; β = β, γ = γ, δ = δ)
        W = W ./ sum(W, dims = 2)
        return (1.0 .- z) .* (-x + W*x) 
    end
    return ForwardDiff.jacobian(x -> dx(x, p), x0)
end

function analyticJacobian(x0, p)
    A, z, β, γ, δ = p
    W, ∂W = weightMatrixWithDeriv(x0, A; β = β, γ = γ, δ = δ)
    s = dropdims(sum(W, dims = 1), dims = 1)

    Σ = Diagonal(1 ./ s)
    Ξ = Diagonal(x0)

    Ĵ = Σ*(∂W*Ξ + W - Σ*Diagonal(W*x0)*∂W)
    for i in 1:length(x0)
        Ĵ[i,i] = -sum(Ĵ[i,:])
        if z[i] == 1
            Ĵ[i,:] .= 0
        end
    end
    return Ĵ
end
