function completeGraph(n)
    A = zeros(n+2, n+2)
    for i ∈ 2:(n+1), j ∈ 2:(i-1) 
        A[i  , j  ] = 1.0
        A[j  , i  ] = 1.0
    end
    A[1, 2:(n+1)]   .= 1.0
    A[2:(n+1), 1]   .= 1.0
    A[n+2, 2:(n+1)] .= 1.0
    A[2:(n+1), n+2] .= 1.0

    x = zeros(n+2)
    x[1]      = 1.0
    x[end]    = -1.0
    
    z = zeros(n+2)
    z[1] = z[end] = 1.0

    return A, x, z
end

function pairedCliques(n)
    
    # create the adjacency matrix
    A = zeros(2n+2, 2n+2)
    
    for i in 2:(n+1), j in 2:i
        if i != j
            A[i  , j  ] = 1.0
            A[j  , i  ] = 1.0
            A[i+n, j+n] = 1.0
            A[j+n, i+n] = 1.0
        end
        A[i  , i+n] = 1.0
        A[i+n, i  ] = 1.0
    end

    A[1           , 2:(2n+1)    ] .= 1.0
    A[2:(2n+1)     , 1          ] .= 1.0

    A[2n+2        ,2:(2n+1)] .= 1.0
    A[2:(2n+1), 2n+2       ] .= 1.0
        
    x = zeros(2n+2)
    x[1:(n+1)]      .= 1.0
    x[(n+2):(2n+2)] .= -1.0
    
    z = zeros(2n+2)
    z[1] = z[end]   = 1.0
    
    return A, x, z
end

function pathGraph(n, x)
    A = zeros(n+2, n+2)

    for i ∈ 1:n+1
        A[i, i+1] = 1
        A[i+1, i] = 1
    end

    z    = zeros(n+2)
    z[1] = z[end] = 1.0

    return A, x, z
end