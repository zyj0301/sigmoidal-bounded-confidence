using StaticArrays

w(x₁, x₂ ; pars...) = logistic_sigmoid(x₁, x₂; β = 1, pars...)

function paired_clique_dynamics(x₁, x₂, dᵢ, dₒ, z₁ = 1, z₂ = -1; pars...)
    w_x₁x₂ = w(x₁, x₂; pars...)
    w_x₁x₁ = w(x₁, x₁; pars...)
    
    w_x₂x₁ = w_x₁x₂
    w_x₂x₂ = w_x₁x₁

    w_x₁z₁ = w(x₁, z₁; pars...)
    w_x₂z₁ = w(x₂, z₁; pars...)
    w_x₁z₂ = w(x₁, z₂; pars...)
    w_x₂z₂ = w(x₂, z₂; pars...)
    
    s₁ = dₒ*w_x₁x₂ + dᵢ*w_x₁x₁ + w_x₁z₁ + w_x₁z₂
    s₂ = dₒ*w_x₂x₁ + dᵢ*w_x₂x₂ + w_x₂z₁ + w_x₂z₂

    dx₁ = (dₒ*w_x₁x₂*(x₂-x₁) + w_x₁z₁*(z₁-x₁) + w_x₁z₂*(z₂-x₁))/s₁
    dx₂ = (dₒ*w_x₂x₁*(x₁-x₂) + w_x₂z₁*(z₁-x₂) + w_x₂z₂*(z₂-x₂))/s₂

    return SVector{2}(dx₁, dx₂)
end

