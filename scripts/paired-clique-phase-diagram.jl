# to run
# julia --project=. --threads 20 scripts/paired-clique-phase-diagram.jl 

using AdaptiveOpinions
include("paired-clique-utils.jl")

using DataFrames
using CSV
using Roots
using ForwardDiff
using LinearAlgebra

base_dir = "throughput/paired-cliques"

if !isdir(base_dir)
    Base.Filesystem.mkpath(base_dir)
end

for suffix ∈ ["phase-diagram", "supplementary"]
    path = base_dir*"/"*suffix
    if isdir(path)
        Base.Filesystem.rm(path, recursive = true)
    end
    Base.Filesystem.mkpath(path)
end

# parameters
n = 10 # number of nodes in each clique

F(X, dᵢ, dₒ; pars...) = paired_clique_dynamics(X[1], X[2], dᵢ, dₒ; pars...)
f(x, dᵢ, dₒ; pars...) = F([x, -x], dᵢ, dₒ; pars...)[1]
# g(x, dᵢ, dₒ; pars...) = paired_clique_dynamics(x, -x, dᵢ, dₒ; pars...)[1]

# for diagrammatic purposes: evaluation of g for a range of parameters

Dᵢ = [5,9,10]

X = -1.1:0.05:1.1

DF = DataFrame()

for dᵢ ∈ Dᵢ, γ ∈ [0, 2, 5, 20], δ ∈ [0, 0.25, 0.5, 0.7, 1.0, 1.5]
    Y = f.(X, dᵢ, n-dᵢ; γ = γ, δ = δ)
    df = DataFrame(gamma = γ, delta = δ, x = X, y = Y, dᵢ = dᵢ)
    global DF = vcat(DF, df)
end
CSV.write(base_dir*"/supplementary/supplementary.csv", DF)


# main computation

Γ = 0:0.05:15
Δ = 0.5:0.002:2.2

Threads.@threads for γ ∈ Γ
    DF = DataFrame()
    for δ ∈ Δ, dᵢ ∈ Dᵢ
        dₒ = n - dᵢ

        roots = find_zeros(x -> f(x, dᵢ, dₒ; γ = γ, δ = δ), -1.1, 1.1)
        J = x -> ForwardDiff.jacobian(s -> F(s, dᵢ, dₒ; γ = γ, δ = δ), x)
        for root ∈ roots
            x = [root, -root]
            j = J(x)
            df = DataFrame(gamma = γ, 
                           delta = δ, 
                           d_i = dᵢ, 
                           x_1 = root,
                           x_2 = -root, 
                           stable = all(real.(eigen(j).values) .<= 0), 
                           id = Threads.threadid())
            DF = vcat(DF, df)
        end
    end
    CSV.write(base_dir*"/phase-diagram/gamma-$γ.csv", DF)
end