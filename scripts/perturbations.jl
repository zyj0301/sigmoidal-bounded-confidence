using Revise
using AdaptiveOpinions
using DataFrames
using CSV

using DifferentialEquations
using RCall
using Roots
using ForwardDiff
using Graphs

### for retrieving solutions

function experiment(γ, δ, x; detailed = false, tspan = 100)
    # initialize topology
    # A, x, z = pathGraph(n, x);
    # containers for dynamical updates
    dx = similar(x)
    W = similar(A)
    # collect parameters
    p = (A, z, β, γ, δ, W)
    # time interval over which to solve
    tspan = (0.0, tspan)
    # solve the ODE
    prob = ODEProblem(dynamics!, x, tspan, p)
    # retrieve the solutions
    sol = solve(prob);
    return sol[end]
end


### initialize problem

g = Graphs.SimpleGraphs.smallgraph(:karate)
A = 1.0*Matrix(adjacency_matrix(g))

n = size(A)[1]
z = zeros(n)
z[1] = z[n] = 1.0

### set parameters

β = 1.0
δ = 0.5

### acquire harmonic solution 

x₀ = vcat(-1, zeros(n-2), 1)            # initial condition
x̄  = experiment(0, δ, x₀; tspan = 1000) # harmonic solution

### acquire Jacobian matrix J
J   = autoJacobian(x̄, (A, z, β, 0.0, δ))
Jₚ  = J[2:(n-1), 2:(n-1)]

### acquire RHS of equation Jϵ = ∂γ(F)
### need to check on this formula
∂F = sum((x̄' .- x̄).*((x̄' .- x̄).^2 .- δ) .* A; dims = 1) ./ sum(A; dims = 1)
∂F = ∂F[2:(n-1)]

### compute the perturbation
### does appear basically plausible, but checks needed
### should be compared against Fig. 3

ϵ  = -Jₚ\∂F

ϵ = vcat(0, ϵ, 0)

### acquire solution at γ = 1.5 or so

γ₀ = 1.85
x  = experiment(γ₀, δ, x₀; tspan = 1000) # harmonic solution

### small gamma family

function run_experiments(Γ)
    x = zeros(n)
    x[1] =  1.0
    x[n] = -1.0

    m = length(Γ)

    # x = experiment(0.0, x; detailed = false)
    X = zeros(m, n)

    for i ∈ 1:m
        x = experiment(Γ[i], δ, x; detailed = false)
        X[i,:] = x
    end
    return X
end

Γ = 0.0:0.005:2.0
X = run_experiments(Γ)




R"""

library(tidyverse)
library(tidygraph)
library(ggraph)
library(colorspace)
library(patchwork)

berlin <- diverging_hcl(9, "Berlin")[c(2,8)]

colors <- c("#f5b895", "#dbdbdb", "#0f4c81")

library(tidyverse)
df <- as_tibble($X) %>%
    mutate(gamma = $Γ) %>% 
    pivot_longer(-gamma, names_to = "name", values_to = "x") %>% 
    mutate(highlight = (x == 1) | (x == -1))

zealots <- df 

persuadables <- df 

w <- persuadables %>% 
    ggplot() + 
    aes(x = gamma, y = x, group = name) + 
    # geom_line(color = "darkslategrey") +
    # geom_line(color = "darkslategrey", linetype = "dashed", data = zealots) +
    geom_point(aes(color = -x), size = .2) +
    theme_bw() + 
    guides(color = "none") + 
    theme(panel.grid.minor = element_blank()) + 
    ylab("Opinion state") + 
    xlab(expression(italic(γ))) + 
    theme(panel.border = element_blank(), 
    axis.line = element_line(size = 0.5, color = "darkgrey"),
    axis.ticks = element_line(color = "darkgrey")) + 
    geom_vline(xintercept = 0, linetype = "dashed") + 
    geom_vline(xintercept = $γ₀, linetype = "dashed") + 
    scale_color_gradientn(colors = colors) +  
    ggtitle("Continuous family containing harmonic solution")

ggsave("fig/karate-small-gamma.png", w, width =4, height = 3, dpi = 600)


g <- as_tbl_graph($A) %>% 
    activate(nodes) %>% 
    mutate(harmonic = $x̄, 
           epsilon = $ϵ,
           later = $x,
           z = row_number() %in% c(1, $n),
           sign = ifelse(epsilon < 0.0001, 0, 1),
           sign = ifelse(epsilon < -0.0001, -1, sign)) 

g <- g %>%
    mutate(random1 = 2*sample(1:2, $n, replace = T)-3, 
           random2 = 2*sample(1:2, $n, replace = T)-3,
           random1 = ifelse(row_number() == 1, -1, random1),
           random1 = ifelse(row_number() == $n, 1, random1),
           random2 = ifelse(row_number() == 1, -1, random2),
           random2 = ifelse(row_number() == $n, 1, random2))

p <- g %>% 
    ggraph() +
    geom_edge_link(color = "grey") +
    geom_node_point(aes(fill = harmonic, stroke = z, shape = factor(harmonic > 0)), size = 6,  color = "black") + 
    scale_fill_gradientn(colors = colors) +  
    scale_shape_manual(values = c(21, 22)) + 
    theme_void() + 
    scale_fill_gradientn(colors = colors) +  
    scale_color_manual(values = c("white", "black")) + 
    scale_discrete_manual(
        aesthetics = "stroke",
        values = c(`FALSE` = 0.5, `TRUE` = 2)
    ) + 
    guides(stroke = "none", fill = "none", shape = "none", shape = "none") + 
    ggtitle("Harmonic solution (γ = 0)") 

s <- g %>% 
    ggraph() +
    geom_edge_link(color = "grey") +
    geom_node_point(aes(fill = sign, stroke = z, shape = factor(harmonic > 0)), size = 6, lwd = 2, color = "black") + 
    theme_void() + 
    scale_fill_gradientn(colors = colors) +  
    scale_color_manual(values = c("white", "black")) + 
    scale_shape_manual(values = c(21, 22)) + 
    ggtitle("Sign of 1st-order perturbation") + 
    scale_discrete_manual(
        aesthetics = "stroke",
        values = c(`FALSE` = 0.5, `TRUE` = 2)
    ) + 
    guides(stroke = "none") 

t <- g %>% 
    ggraph() +
    geom_edge_link(color = "grey") +
    geom_node_point(aes(fill = later, stroke = z, shape = factor(harmonic > 0)), size = 6, lwd = 2) + 
    theme_void() + 
    scale_fill_gradientn(colors = colors) + 
    # scale_fill_continuous_diverging(colormap, limits = c(-1.2, 1.2)) + 
    scale_color_manual(values = c("white", "black")) + 
    scale_shape_manual(values = c(21, 22)) + 
    ggtitle(paste0("γ = ", $γ₀)) + 
    scale_discrete_manual(
        aesthetics = "stroke",
        values = c(`FALSE` = 0.5, `TRUE` = 2)
    ) + 
    guides(stroke = "none", fill = guide_colorbar(title = "Opinion"), shape = "none") 

layout <- "
    AAABBB
    CCCCCD
"

q <- p + t  + w + guide_area() + plot_layout(guides = "collect", design = layout)  

ggsave("fig/karate.png", q, width = 8, height = 6.5)
"""

# two random figs for slides

R"""
random1 <- g %>% 
    ggraph() +
    geom_edge_link(color = "grey") +
    geom_node_point(aes(fill = random1, stroke = z), size = 6, lwd = 2, pch = 21) + 
    theme_void() + 
    scale_fill_gradientn(colors = colors) + 
    # scale_fill_continuous_diverging(colormap, limits = c(-1.2, 1.2)) + 
    scale_color_manual(values = c("white", "black")) + 
    scale_discrete_manual(
        aesthetics = "stroke",
        values = c(`FALSE` = 0.5, `TRUE` = 2)
    ) + 
    guides(stroke = "none", fill = "none", shape = "none") 

    ggsave("fig/karate-random-1.png", random1, width = 4, height = 3)

"""

R"""
random2 <- g %>% 
    ggraph() +
    geom_edge_link(color = "grey") +
    geom_node_point(aes(fill = random2, stroke = z), size = 6, lwd = 2, pch = 21) + 
    theme_void() + 
    scale_fill_gradientn(colors = colors) + 
    # scale_fill_continuous_diverging(colormap, limits = c(-1.2, 1.2)) + 
    scale_color_manual(values = c("white", "black")) + 
    scale_discrete_manual(
        aesthetics = "stroke",
        values = c(`FALSE` = 0.5, `TRUE` = 2)
    ) + 
    guides(stroke = "none", fill = "none", shape = "none") 

    ggsave("fig/karate-random-2.png", random2, width = 4, height = 3)

"""

R"""
harmonic <- g %>% 
    ggraph() +
    geom_edge_link(color = "grey") +
    geom_node_point(aes(fill = harmonic, stroke = z), size = 6, lwd = 2, pch = 21) + 
    theme_void() + 
    scale_fill_gradientn(colors = colors) + 
    # scale_fill_continuous_diverging(colormap, limits = c(-1.2, 1.2)) + 
    scale_color_manual(values = c("white", "black")) + 
    scale_discrete_manual(
        aesthetics = "stroke",
        values = c(`FALSE` = 0.5, `TRUE` = 2)
    ) + 
    guides(stroke = "none", fill = "none", shape = "none") 

    ggsave("fig/karate-harmonic.png", harmonic, width = 4, height = 3)
"""
