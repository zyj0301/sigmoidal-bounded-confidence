# to run
# julia --project=. --threads 20 scripts/path-graph-phase-diagram.jl 

using Revise
using AdaptiveOpinions
using DataFrames
using CSV

using DifferentialEquations
using RCall
using Roots
using ForwardDiff
# using StaticArrays

# using IntervalArithmetic, IntervalRootFinding

# remove old throughput and regenerate the needed directory

for i ∈ [1, 2]
    path = "throughput/path-graph-$i"
    Base.Filesystem.rm(path, recursive = true, force = true)
    Base.Filesystem.mkpath(path)
end

n = 10

######################
# PLATEAU-Y SOLUTIONS
######################

u(s, n; pars...) = logistic_sigmoid((n-1)/2*s, (n+1)/2; β = 1, pars...)
v(s, n; pars...) = logistic_sigmoid(s, 0; β = 1, pars...)

g(s, n; pars...) = (u(s, n; pars...)*((n+1)/2 - (n-1)/2*s) + v(s, n; pars...)*(-s))/(u(s, n; pars...) + v(s, n; pars...))

Γ = 0:0.002:3
Δ = 0.0:0.02:3.0

Threads.@threads for γ ∈ Γ
    DF = DataFrame()
    for δ ∈ Δ
        f(s) = g(s, n; γ = γ, δ = δ)
        # X = roots(f, -1..1)
        X = find_zeros(f, 0, 2.0)
        
        h = x -> ForwardDiff.derivative(s -> g(s, n; γ = γ, δ = δ), x)

        df = DataFrame(gamma = γ, delta = δ, x = X, d = h.(X), id = Threads.threadid())
        DF = vcat(DF, df)
    end
    CSV.write("throughput/path-graph-1/gamma-$γ.csv", DF)
end


######################
# POLARIZED SOLUTIONS
######################

# between-group
u(s, n; pars...) = logistic_sigmoid(2*((n+1)/2 - (n/2)*s), 0; β = 1, pars...)

# within-group
v(s, n; pars...) = logistic_sigmoid(s, 0; β = 1, pars...)

g(s, n; pars...) = (u(s, n; pars...)*2*((n+1)/2 - (n/2)*s) + v(s, n; pars...)*(-s))/(u(s, n; pars...) + v(s, n; pars...))


Γ = 0:0.002:3
Δ = 0.0:0.02:3.0

Threads.@threads for γ ∈ Γ
    DF = DataFrame()
    for δ ∈ Δ
        f(s) = g(s, n; γ = γ, δ = δ)
        # X = roots(f, -1..1)
        X = find_zeros(f, 0, 2.0)
        
        h = x -> ForwardDiff.derivative(s -> g(s, n; γ = γ, δ = δ), x)

        df = DataFrame(gamma = γ, delta = δ, x = X, d = h.(X), id = Threads.threadid())
        DF = vcat(DF, df)
    end
    CSV.write("throughput/path-graph-2/gamma-$γ.csv", DF)
end


# parameters
n = 10
x₀ = vcat(-(n+1)/2, zeros(n), (n+1)/2)
A, x, z = pathGraph(n, x₀)
β = 1.0

function experiment(γ, δ, x; detailed = false, tspan = 100)
    # initialize topology
    # A, x, z = pathGraph(n, x);
    # containers for dynamical updates
    dx = similar(x)
    W = similar(A)
    # collect parameters
    p = (A, z, β, γ, δ, W)
    # time interval over which to solve
    tspan = (0.0, tspan)
    # solve the ODE
    prob = ODEProblem(dynamics!, x, tspan, p)
    # retrieve the solutions
    sol = solve(prob);
    return sol[end]
end


ϵ = 0.00001
γ = 60
δ = .01

v_ = vcat(-1, -1*ones(n÷2), 1*ones(n÷2), 1)*(n+1)/2
x = (-(n+1)/2):1:((n+1)/2) |> collect
x = (1-ϵ)*x + ϵ*v_
y = experiment(γ, δ, x; tspan = 5E16)


function w_(x₁, x₂; γ, δ)
    1 / (1 + exp(γ*(x₁ - x₂)^2 - γ*δ))
end


(w_(y[1], y[2]; γ, δ)*(y[1] - y[2]) + (w_(y[3], y[2]; γ, δ)*(y[3] - y[2]))) 


# R"""
# library(tidyverse)

# tibble(y = $y) %>% 
#     mutate(n = row_number()) %>% 
#     ggplot() + 
#     aes(x = n, y = y) + 
#     geom_point() 

# ggsave("fig/test.png")
# """

# # y[6] - y[7]
# # y[5] - y[6]
# # y[4] - y[5]
# # y[3] - y[4]
# y[2] - y[3]






