using Revise
using AdaptiveOpinions # custom package. Requires pkg> activate .
using GraphPlot
using Colors
using StatsBase
using LinearAlgebra


######
# COLORS
######

n_colors = 30
color_pal = range(colorant"lightseagreen", stop=colorant"orange", length=n_colors)
edge_pal = range(colorant"white", stop=colorant"gray", length=n_colors)

######
# GRAPH + GRAPH VIZ
######


G = graphfamous("polbooks")
n = nv(G)
locs_x, locs_y = spring_layout(G)

######
# PARAMETERS
######

λ = 0.5
γ = 3.3
η = 0.01
β = 1.0

f(x1, x2) = logistic_sigmoid(x1, x2; β = β, γ = γ, δ = 0.5)
g(x) = x .+ η .* x .* (1 .- x.^2)

######
# INITIALIZATION
######

S = randomInitialization(G, 1)
for a in S.agents a.λ = λ end
for i in vertices(G) S.agents[i].x = i < 50 ? [1.0] : [-1.0] end

######
# DYNAMICS
######

for i = 1:5000 evolve!(S, f, g) end


######
# EXTRACT DATA
######

X = stateVector(S)
X = round.(X, digits = 8)

node_colors = [color_pal[trunc(Int, 1.0+(n_colors-1.0)*(x[1]+1.0)/2.0)] for x in X]

edge_weights = repeat([0.0], ne(G))
E = collect(edges(G))
for i in 1:length(E)
      e = E[i]
      edge_weights[i] = f(S.agents[src(e)].x, S.agents[dst(e)].x)
end

edge_colors = [edge_pal[trunc(Int, 1+(n_colors-1)*(w))] for w in edge_weights]

p = gplot(S.G, locs_x, locs_y,
      nodefillc=node_colors,
      # edgestrokec = edge_colors,
      edgelinewidth = .5*edge_weights .+ 0.2)

p

J = numericalJacobian(x, f, g; G = G)
ν = maximum(abs.(eigvals(J)))
ν < 1.0
